package com.restapi.Users;

import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@Controller
@RequestMapping(path = "/api")
public class UsersController {


    @Autowired
    private UserRepo userRepo;

    @Autowired
    private QueryRepo queryRepo;


    @GetMapping(path="/users")
    @ResponseBody
    public Object getAllUsers() {
        return userRepo.findAll();
    }

    @GetMapping(path = "/users/count")
    @ResponseBody
    public Object getIds()
    {
        return queryRepo.getId();
    }

    @RequestMapping(path = "/users/add", method = RequestMethod.POST)
    @ResponseBody
    public Object postUsers(@RequestBody Map<String, Object> data)
    {
        Users user = new Users();

        user.setName(data.get("name").toString());
        user.setEmail(data.get("email").toString());
        user.setPosition(data.get("position").toString());

        userRepo.save(user);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "User was saved to data base");
        jsonObject.addProperty("status", "Saved");

        return jsonObject.toString();
    }


}
