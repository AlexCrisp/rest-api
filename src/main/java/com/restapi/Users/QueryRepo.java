package com.restapi.Users;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import java.util.List;

public interface QueryRepo extends CrudRepository<Users, Integer> {


    @ManyToMany(fetch = FetchType.LAZY)
    @Query(
            value = "select count(u.id) from users u",
            nativeQuery =  true
    )
    List<Object[]> getId();

}
