package com.restapi.Users;


import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Users {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull
    private String name;

    @NotNull
    private String email;

    @NotNull
    private String position;

    public long getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String setName(String name) {
        this.name = name;
        return this.name;
    }

    public String getEmail() {
        return email;
    }

    public String setEmail(String email) {
        this.email = email;
        return this.email;
    }

    public String getPosition() {
        return position;
    }

    public String setPosition(String position) {
        this.position = position;
        return this.position;
    }

}
