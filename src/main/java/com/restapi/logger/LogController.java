package com.restapi.logger;


import com.google.gson.JsonObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@Controller
@RequestMapping(path = "/api")
public class LogController {

    @Autowired
    private LogRepo logRepo;

    @GetMapping(value = "/log")
    @ResponseBody
    public Object getLog()
    {
        return logRepo.findAll();
    }

    @RequestMapping(path = "/log/add", method = RequestMethod.POST)
    @ResponseBody
    public Object postUsers(@RequestBody Map<String, Object> data)
    {
        Log log = new Log();

        log.setLogger(data.get("logger").toString());

        logRepo.save(log);

        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("message", "Log was saved to data base");
        jsonObject.addProperty("status", "Saved");

        return jsonObject.toString();
    }

}
