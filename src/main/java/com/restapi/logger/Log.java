package com.restapi.logger;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @NotNull
    private String logger;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogger() {
        return logger;
    }

    public String setLogger(String logger) {
        this.logger = logger;
        return this.logger;
    }
}
