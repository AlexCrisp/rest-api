package com.restapi.logger;

import org.springframework.data.repository.CrudRepository;

public interface LogRepo extends CrudRepository<Log, Integer> {
}
