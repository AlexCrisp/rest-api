<H1>RestAPI</H1>
<p>Simple url description:</p>
<hr>

1. Url = /api/users - returned all users from table users
<hr>
2. Url = /api/users/count - returned counting users from table users 
<hr>
3. Url = /api/log - returned all logs from table log
<hr>
4. Url = /api/log/add - return message if saved log, params:
    
    {
    	"logger": "message"
    }
    
    
Result:

    {
        "message":"Log was saved to data base",
        "status":"Saved"
    }
    
<hr>
5. Url = /api/users/add - return message if saved new user in table, params:

    {
	    "name":"TestUser",
	    "email":"testUserEmail@email",
	    "position":"TestPosition"
    }

Result:

    {
        "message":"User was saved to data base",
        "status":"Saved"
    }